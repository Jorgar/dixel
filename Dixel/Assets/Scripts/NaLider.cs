﻿using UnityEngine;
using System.Collections;

public class NaLider : MonoBehaviour {

	public static bool pistoletazoDerecha = false;
	public static bool pistoletazoIzquierda = false;
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.transform.position.x >= 20.46f) {
						pistoletazoIzquierda = true;
						StartCoroutine(esperarUno());
				}
		if (this.gameObject.transform.position.x <= -20.46f) {
						pistoletazoDerecha = true;
						StartCoroutine(esperarDos());
				}
	}

	IEnumerator esperarUno(){
		yield return new WaitForSeconds(0.5f);
		pistoletazoIzquierda = false;
	}

	IEnumerator esperarDos(){
		yield return new WaitForSeconds(0.5f);
		pistoletazoDerecha = false;
	}
}
