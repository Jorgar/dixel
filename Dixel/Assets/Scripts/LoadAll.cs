﻿using UnityEngine;
using System.Collections;

public class LoadAll : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Data.Load ();
		Time.timeScale = 1;
		RestartButton.siMusicalvl12 = false;
		RestartButton.noCabecera = false;
		RestartButton.noTimewait = false;
		if(MuteAll.isMute)
			AudioListener.volume = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
