﻿using UnityEngine;
using System.Collections;

public class MovimientoCubosMM : MonoBehaviour {

	public GameObject cube1, cube2, cube3;
	public int velocidad;
	bool m1,m2,m3;
	Vector3 positionCube1,positionCube2,positionCube3;


	void Start(){
		 positionCube1 = cube1.transform.position;
		 positionCube2 = cube2.transform.position;
		 positionCube3 = cube3.transform.position;
		 m1 = true;
	}

	void Update(){
		StartCoroutine (tiempo ());
	}

	IEnumerator tiempo(){
		if (m1) {
			cube1.transform.Translate (Vector3.right * velocidad * Time.deltaTime, Space.World);
			if (cube1.transform.position.x > 3000) {
				m1 = false;
				m2 = true;
			} 
		}
		yield return new WaitForSeconds (3);
		if(m2){
			cube2.transform.Translate (Vector3.up * velocidad * Time.deltaTime, Space.World);
			if (cube2.transform.position.y > 1169) {
				m2 = false;
				m3 = true;
			} 
		}
		yield return new WaitForSeconds (3);
		if(m3){
			cube3.transform.Translate (Vector3.down* velocidad * Time.deltaTime, Space.World);
			if (cube3.transform.position.y < (-1169)) {
				m3 = false;
				cube1.transform.position = positionCube1;
				cube2.transform.position = positionCube2;
				cube3.transform.position = positionCube3;
				m1 = true;
				StartCoroutine (tiempo ());
			} 
		}
	}
}
