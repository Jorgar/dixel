﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeSceneUp : MonoBehaviour {

    public void toMainMenu()
    {
        StopAllCoroutines();
        SceneManager.LoadScene("01_Main");
    }

    public void toLevel2()
    {
        StopAllCoroutines();
        SceneManager.LoadScene("Level_2");
    }

    public void toLevel3()
    {
        SceneManager.LoadScene("Level_3");
        StopAllCoroutines();
    }
    public void toLevel4()
    {
        SceneManager.LoadScene("Level_4");
        StopAllCoroutines();
    }

    public void toLevel5()
    {
        SceneManager.LoadScene("Level_5"); ;
        StopAllCoroutines();
    }

    public void toLevel6()
    {
        SceneManager.LoadScene("Level_6");
        StopAllCoroutines();
    }

    public void toLevel7()
    {
        SceneManager.LoadScene("Level_7");
        StopAllCoroutines();
    }

    public void toLevel8()
    {
        SceneManager.LoadScene("Level_8");
        StopAllCoroutines();
    }

    public void toLevel9()
    {
        SceneManager.LoadScene("Level_9");
        StopAllCoroutines();
    }

    public void toLevel10()
    {
        SceneManager.LoadScene("Level_10");
        StopAllCoroutines();
    }

    public void toLevel11()
    {
        SceneManager.LoadScene("Level_11");
        StopAllCoroutines();
    }

    public void toLevel12()
    {
        SceneManager.LoadScene("Level_12");
        StopAllCoroutines();
    }

    public void toLevel13()
    {
        SceneManager.LoadScene("Level_13");
        StopAllCoroutines();
    }

    public void toLevel14()
    {
        SceneManager.LoadScene("Level_14");
        StopAllCoroutines();
    }

    public void toLevel15()
    {
        StopAllCoroutines();
        SceneManager.LoadScene("Level_15");

    }

    public void toLevel16()
    {
        StopAllCoroutines();
        SceneManager.LoadScene("Level_16");

    }


}
