﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class tecleoLoadGene : MonoBehaviour {

    public AudioSource uno, dos, tres, cuatro, cinco, enter;
    AudioSource[] tecla;
    [Multiline(6)]
    public string frase;
    string cuentaLoad;
    public TextMesh mensaje1, loading;
    public float segundos;
    public GameObject par, m2;
    public float separacion;
    public GameObject barra;
    bool cargar;

    // Use this for initialization
    void Start()
    {
        tecla = new AudioSource[5];
        cargar = false;
        tecla[0] = uno;
        tecla[1] = dos;
        tecla[2] = tres;
        tecla[3] = cuatro;
        tecla[4] = cinco;
        StartCoroutine(escritura());

    }

    // Update is called once per frame
    void Update()
    {

        if (cargar)
        {
            if (ChangeScene.level1Load)
            {
                ChangeScene.level1Load = false;
                SceneManager.LoadScene("Level_1");
            }
            else if (ChangeScene.level2Load)
            {
                ChangeScene.level2Load = false;
                SceneManager.LoadScene("Level_2");
            }
            else if (ChangeScene.level3Load)
            {
                ChangeScene.level3Load = false;
                SceneManager.LoadScene("Level_3");
            }
            else if (ChangeScene.level4Load)
            {
                ChangeScene.level4Load = false;
                SceneManager.LoadScene("Level_4");
            }
            else if (ChangeScene.level5Load)
            {
                ChangeScene.level5Load = false;
                SceneManager.LoadScene("Level_5");
            }
            else if (ChangeScene.level6Load)
            {
                ChangeScene.level6Load = false;
                SceneManager.LoadScene("Level_6");
            }
            else if (ChangeScene.level7Load)
            {
                ChangeScene.level7Load = false;
                SceneManager.LoadScene("Level_7");
            }
            else if (ChangeScene.level8Load)
            {
                ChangeScene.level8Load = false;
                SceneManager.LoadScene("Level_8");
            }
            else if (ChangeScene.level9Load)
            {
                ChangeScene.level9Load = false;
                SceneManager.LoadScene("Level_9");
            }
            else if (ChangeScene.level10Load)
            {
                ChangeScene.level10Load = false;
                SceneManager.LoadScene("Level_10");
            }
            else if (ChangeScene.level11Load)
            {
                ChangeScene.level11Load = false;
                SceneManager.LoadScene("Level_11");
            }
            else if (ChangeScene.level12Load)
            {
                ChangeScene.level12Load = false;
                SceneManager.LoadScene("Level_12");
            }
            else if (ChangeScene.level13Load)
            {
                ChangeScene.level13Load = false;
                SceneManager.LoadScene("Level_13");
            }
            else if (ChangeScene.level14Load)
            {
                ChangeScene.level14Load = false;
                SceneManager.LoadScene("Level_14");
            }
            else if (ChangeScene.level15Load)
            {
                ChangeScene.level15Load = false;
                SceneManager.LoadScene("Level_15");
            }
            else if (ChangeScene.level16Load)
            {
                ChangeScene.level16Load = false;
                SceneManager.LoadScene("Level_16");
            }
        }
    }

    IEnumerator escritura()
    {
        Debug.Log("tecleo1");
        for (int i = 0; i < frase.Length; i++)
        {
            Debug.Log("tecleo2");
            yield return new WaitForSeconds(0.1f);
            Debug.Log("tecleo3");
            mensaje1.text += frase[i];
            Debug.Log("tecleo4");
            par.transform.position = new Vector2(par.transform.position.x + separacion, par.transform.position.y);
            Debug.Log("tecleo5");
            tecla[(int)Random.Range(0, 4)].Play();
        }

        enter.Play();
        yield return new WaitForSeconds(0.2f);
        barra.SetActive(true);

        par.transform.position = new Vector2(-280.92f, 115.6f);

        yield return new WaitForSeconds(0.25f);

        for (int j = 1; j <= 100; j++)
        {
            cuentaLoad = j + "%";
            loading.text = cuentaLoad;
            yield return new WaitForSeconds(0.015f);
        }
        
        cargar = true;

    }

    
}

