﻿using UnityEngine;
using System.Collections;

public class Desbloqueo2 : MonoBehaviour {

	public GameObject level5Locked, level5, level5Complete;
	public GameObject level6Locked, level6, level6Complete;
	public GameObject level7Locked, level7, level7Complete;
	public GameObject level8Locked, level8, level8Complete;
	
	void Start () {
		if (Application.loadedLevelName.Equals ("03_Levels02")) {
						if (Controlador.level4Complete == true) {
								level5Locked.SetActive (false);
								level5.SetActive (true);	
						}
						if (Controlador.level5Complete == true) {
								level5.SetActive (false);
								level5Complete.SetActive (true);
								level6Locked.SetActive (false);
								level6.SetActive (true);	
						}
		
						if (Controlador.level6Complete == true) {
								level6.SetActive (false);
								level6Complete.SetActive (true);
								level7Locked.SetActive (false);
								level7.SetActive (true);	
						}
						if (Controlador.level7Complete == true) {
								level7.SetActive (false);
								level7Complete.SetActive (true);
								level8Locked.SetActive (false);
								level8.SetActive (true);	
						}
						if (Controlador.level8Complete == true) {
								level8.SetActive (false);
								level8Complete.SetActive (true);
						}
		} else if (Application.loadedLevelName.Equals ("03_Levels03")) {
			if (Controlador.level8Complete == true) {
				level5Locked.SetActive (false);
				level5.SetActive (true);	
			}
			if (Controlador.level9Complete == true) {
				level5.SetActive (false);
				level5Complete.SetActive (true);
				level6Locked.SetActive (false);
				level6.SetActive (true);	
			}
			
			if (Controlador.level10Complete == true) {
				level6.SetActive (false);
				level6Complete.SetActive (true);
				level7Locked.SetActive (false);
				level7.SetActive (true);	
			}
			if (Controlador.level11Complete == true) {
				level7.SetActive (false);
				level7Complete.SetActive (true);
				level8Locked.SetActive (false);
				level8.SetActive (true);	
			}
			if (Controlador.level12Complete == true) {
				level8.SetActive (false);
				level8Complete.SetActive (true);
			}

		}else if(Application.loadedLevelName.Equals ("03_Levels04")) {
			if (Controlador.level12Complete == true) {
				level5Locked.SetActive (false);
				level5.SetActive (true);	
			}
			if (Controlador.level13Complete == true) {
				level5.SetActive (false);
				level5Complete.SetActive (true);
				level6Locked.SetActive (false);
				level6.SetActive (true);	
			}
			
			if (Controlador.level14Complete == true) {
				level6.SetActive (false);
				level6Complete.SetActive (true);
				level7Locked.SetActive (false);
				level7.SetActive (true);	
			}
			if (Controlador.level15Complete == true) {
				level7.SetActive (false);
				level7Complete.SetActive (true);
				level8Locked.SetActive (false);
				level8.SetActive (true);	
			}
		
		}
			

		
	}
}
