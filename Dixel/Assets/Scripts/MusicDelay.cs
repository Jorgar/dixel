﻿using UnityEngine;
using System.Collections;

public class MusicDelay : MonoBehaviour {

	public AudioSource cancion;

	void Start () {
        cancion.GetComponent<AudioSource>();
		StartCoroutine (musica ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator musica(){
        if (RestartButton.siMusicalvl12 == false)
        {
            yield return new WaitForSeconds(0f);
            cancion.Play();
        }
        else {
            if(!(Application.loadedLevelName.Equals("Level_7")))
                Destroy(this.gameObject);
                cancion.Play();
		}
	}
}
