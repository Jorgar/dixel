﻿using UnityEngine;
using System.Collections;

public class Parpadeo : MonoBehaviour {

	public GameObject barra;
	// Use this for initialization
	void Start () {
		StartCoroutine (parpadeo ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator parpadeo(){
		yield return new WaitForSeconds(0.75f);
		barra.SetActive (false);
		yield return new WaitForSeconds(0.75f);
		barra.SetActive (true);
		StartCoroutine (parpadeo ());
	}
}
