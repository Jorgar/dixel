﻿using UnityEngine;
using System.Collections;

public class OpositeMovlvl14 : MonoBehaviour {

	void Start () {
		StartCoroutine (rotacion ());
	}
	
	IEnumerator rotacion(){
		yield return new WaitForSeconds (1f);
		this.transform.eulerAngles = new Vector3(0,0,270);
		yield return new WaitForSeconds (1f);
		this.transform.eulerAngles = new Vector3(0,0,180);
		yield return new WaitForSeconds (1f);
		this.transform.eulerAngles = new Vector3(0,0,90);
		yield return new WaitForSeconds (1f);
		this.transform.eulerAngles = new Vector3(0,0,0);
		StartCoroutine (rotacion ());
	}
}
