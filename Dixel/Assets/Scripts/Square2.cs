﻿using UnityEngine;
using System.Collections;

public class Square2 : MonoBehaviour {

	public GameObject square1, square2, square3;
	public GameObject tap;
	public SpriteRenderer loadingSprite;
	public static bool tapPulsador = false;
	public static bool intTap = false;
	int contador = 0;
	public  int contadora = 0;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		square1.SetActive (false);
		square2.SetActive (false);
		square3.SetActive (false);
		tap.SetActive (false);
		StartCoroutine (tiempoSquare ());
	}
	
	// Update is called once per frame
	void Update () {
				if (intTap) {
						if (Input.GetMouseButtonDown (0)) {
								Application.LoadLevel ("Level_1");
						}			

				}
	}

	public IEnumerator tiempoSquare(){
		while (contador!=3) {
			square1.SetActive (false);
			square2.SetActive (false);
			square3.SetActive (false);
			yield return new WaitForSeconds (0.3f);
			square1.SetActive (true);
			yield return new WaitForSeconds (0.3f);
			square2.SetActive (true);
			yield return new WaitForSeconds (0.3f);
			square3.SetActive (true);
			yield return new WaitForSeconds (0.3f);
			contador++;
		}
		ChangeScene.level1FirstLoad = false;
		Data.Save();
		loadingSprite.color = new Color (1f, 1f, 1f, 0.80f);
		square1.GetComponent<Renderer>().material.color = new Color(1,1,1,0.80f);
		square2.GetComponent<Renderer>().material.color = new Color(1,1,1,0.80f);
		square3.GetComponent<Renderer>().material.color = new Color(1,1,1,0.80f);
		yield return new WaitForSeconds (0.05f);
		loadingSprite.color = new Color (1f, 1f, 1f, 0.60f);
		square1.GetComponent<Renderer>().material.color = new Color(1,1,1,0.60f);
		square2.GetComponent<Renderer>().material.color = new Color(1,1,1,0.60f);
		square3.GetComponent<Renderer>().material.color = new Color(1,1,1,0.60f);
		yield return new WaitForSeconds (0.05f);
		loadingSprite.color = new Color (1f, 1f, 1f, 0.40f);
		square1.GetComponent<Renderer>().material.color = new Color(1,1,1,0.40f);
		square2.GetComponent<Renderer>().material.color = new Color(1,1,1,0.40f);
		square3.GetComponent<Renderer>().material.color = new Color(1,1,1,0.40f);
		yield return new WaitForSeconds (0.05f);
		loadingSprite.color = new Color (1f, 1f, 1f, 0.20f);
		square1.GetComponent<Renderer>().material.color = new Color(1,1,1,0.20f);
		square2.GetComponent<Renderer>().material.color = new Color(1,1,1,0.20f);
		square3.GetComponent<Renderer>().material.color = new Color(1,1,1,0.20f);
		yield return new WaitForSeconds (0.05f);
		loadingSprite.color = new Color (1f, 1f, 1f, 0.00f);
		square1.GetComponent<Renderer>().material.color = new Color(1,1,1,0.00f);
		square2.GetComponent<Renderer>().material.color = new Color(1,1,1,0.00f);
		square3.GetComponent<Renderer>().material.color = new Color(1,1,1,0.0f);
		yield return new WaitForSeconds (0.05f);
		intTap = true;
		tapPulsador = true;
		yield return StartCoroutine(tiempoTap ());
	}

	public  IEnumerator tiempoTap(){
		while (contadora == 0) {
			tap.SetActive(true);
			yield return new WaitForSeconds (0.5f);
			yield return new WaitForSeconds (0.5f);
			yield return new WaitForSeconds (0.5f);
			tap.SetActive(false);
			yield return new WaitForSeconds (0.5f);
		}
	}
}
