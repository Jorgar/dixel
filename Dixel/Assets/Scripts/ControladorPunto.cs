﻿using UnityEngine;
using System.Collections;

public class ControladorPunto : MonoBehaviour {

	public float dirX,dirY;
	public static float posPunto;
	float speed;
	int puntosJugador = 0;
	int puntosEnemigo = 0;
	public int quienSaca = 1;
	public AudioSource sonidoPong;
	public AudioSource marcar;
	public static bool emp = true;
	public static bool emp2 = true;
	public static bool emp3 = true;

	public TextMesh scoreEnemigo;
	public TextMesh scoreJugador;
	// Use this for initialization
	void Start () {
		emp = true;
		emp3 = true;
		emp2 = false;
		if(emp)
			StartCoroutine (empezar ());
		if(emp3 == false){
		this.gameObject.transform.position = new Vector2 (0, 0);
		if(quienSaca ==1){
			dirX = 20;
			dirY = 20;
		}else if(quienSaca == 0){
			dirX = -20;
			dirY = 20;
		}
		GetComponent<Rigidbody2D>().velocity = new Vector2 (dirX, dirY);
		GetComponent<Rigidbody2D>().fixedAngle = true;
		}
	}

	void auxStart () {
		if(emp)
			StartCoroutine (empezar ());
		if(emp3 ==false){
			this.gameObject.transform.position = new Vector2 (0, 0);
			if(quienSaca ==1){
				dirX = 20;
				dirY = 20;
			}else if(quienSaca == 0){
				dirX = -20;
				dirY = 20;
			}
			GetComponent<Rigidbody2D>().velocity = new Vector2 (dirX, dirY);
			GetComponent<Rigidbody2D>().fixedAngle = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(emp3 ==false){
		posPunto = this.gameObject.transform.position.y;
		speed = GetComponent<Rigidbody2D>().velocity.magnitude;

		if(speed < 0.2) {
			GetComponent<Rigidbody2D>().velocity = new Vector2 (-20, 20);
			Debug.Log("esta parado");
		}
		}
	}

	void OnCollisionEnter2D(Collision2D coll){

		if (coll.gameObject.name == "Margen") {
						if (dirX > 0 && dirY > 0) {
								dirY *= -1;
						}else if (dirX < 0 && dirY > 0) {
								dirY *= -1;
						}else if (dirX > 0 && dirY < 0) {
								dirY *= -1;
						}else if (dirX < 0 && dirY < 0) {
								dirY *= -1;
						}
						sonidoPong.Play ();
						GetComponent<Rigidbody2D>().velocity = new Vector2 (dirX, dirY);

		} else if (coll.gameObject.name == "Jugador") {
				if (dirX > 0 && dirY > 0) 
						dirX*= -1;
				else if (dirX > 0 && dirY < 0)
						dirX *= -1;
				sonidoPong.Play ();
				GetComponent<Rigidbody2D>().velocity = new Vector2 (dirX, dirY);
		} else if (coll.gameObject.name == "Enemigo") {
			if (dirX < 0 && dirY > 0) 
				dirX *= -1;
			else if (dirX < 0 && dirY < 0)
				dirX *= -1;
			sonidoPong.Play ();
			GetComponent<Rigidbody2D>().velocity = new Vector2 (dirX, dirY);
		}

		if (coll.gameObject.name == "TriggerJugador") {
			puntosEnemigo++;
			quienSaca = 1;
			scoreEnemigo.text = "" + puntosEnemigo;
			marcar.Play();
			auxStart();
		}else if (coll.gameObject.name == "TriggerEnemigo") {
			puntosJugador++;
			quienSaca = 0;
			scoreJugador.text = "" + puntosJugador;
			marcar.Play();
			auxStart();
		}
	}

	IEnumerator empezar(){
		yield return new WaitForSeconds(0f);
			emp2 = true;
		yield return new WaitForSeconds(1f);
			emp = false;
			emp3 = false;
		auxStart ();
	}

}
