﻿using UnityEngine;
using System.Collections;

public class Controlador : MonoBehaviour {
	public static Vector2 mov;
	public int vel = 5; //vel = 10 es lo ideal

	bool espera = true;
	public static bool movPlayer = true;
	public static bool movAfterDeath = false;
	public AudioSource hit;
	public AudioSource conseguirPuntos;
	public AudioSource ganar;
	public static int puntos = 0;
	public static bool iniSoundMain = false;
	public static bool level1Complete = false;
	public static bool level2Complete = false;
	public static bool level3Complete = false;
	public static bool level4Complete = false;
	public static bool level5Complete = false;
	public static bool level6Complete = false;
	public static bool level7Complete = false;
	public static bool level8Complete = false;
	public static bool level9Complete = false;
	public static bool level10Complete = false;
	public static bool level11Complete = false;
	public static bool level12Complete = false;
	public static bool level13Complete = false;
	public static bool level14Complete = false;
	public static bool level15Complete = false;
	public static bool level16Complete = false;
	public static int life;
	public GameObject jugador;
	public GameObject life1;
	public GameObject life2;
	public GameObject life3;
	public GameObject gameOver;
	public GameObject quit;
	public GameObject complete;
	public static Vector2 auxMov; 
	// SWIPE

	private Vector3 fp; //First finger position
	private Vector3 lp; //Last finger position
	public float dragDistance;  //Distance needed for a swipe to register



	IEnumerator OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "point") {
			puntos ++;
			conseguirPuntos.Play ();
			other.gameObject.SetActive(false);
			if(Application.loadedLevelName.Equals("Level_7")){
				if(puntos == 8){
					RestartButton.siMusicalvl12 = false;
					RestartButton.noCabecera = false;
					RestartButton.noTimewait = false;
					Time.timeScale=0;
					ganar.Play ();
					desbloquearNivel ();
					complete.SetActive (true);
					Quit.continuar = false;
					Data.Save();
				} 
			}
            else if (Application.loadedLevelName.Equals("Level_15"))
            {
                if (puntos == 5)
                {
                    Time.timeScale = 0;
                    ganar.Play();
                    desbloquearNivel();
                    complete.SetActive(true);
                    Quit.continuar = false;
                    Data.Save();
                }
            }
            else if(Application.loadedLevelName.Equals("Level_11")){
				if(puntos == 3){
				Time.timeScale=0;
				ganar.Play ();
				desbloquearNivel ();
				complete.SetActive (true);
				Quit.continuar = false;
				Data.Save();
				} 
			}
            else if (Application.loadedLevelName.Equals("Level_14"))
            {
                if (puntos == 2)
                {
                    Time.timeScale = 0;
                    ganar.Play();
                    desbloquearNivel();
                    complete.SetActive(true);
                    Quit.continuar = false;
                    Data.Save();
                }
            }
            else if (Application.loadedLevelName.Equals("Level_9"))
            {
                if (puntos == 6)
                {
                    Time.timeScale = 0;
                    ganar.Play();
                    desbloquearNivel();
                    complete.SetActive(true);
                    Quit.continuar = false;
                    Data.Save();
                }
            }
            else if(Application.loadedLevelName.Equals("Level_8")){
				if(puntos == 8){
					Time.timeScale=0;
					ganar.Play ();
					desbloquearNivel ();
					complete.SetActive (true);
					Quit.continuar = false;
					Data.Save();
				} 
			} else {
			if(puntos == 10){
				Time.timeScale=0;
				ganar.Play ();
				desbloquearNivel ();
				complete.SetActive (true);
				Quit.continuar = false;
				Data.Save();
			} 
		}
		}else if(other.gameObject.tag == "siguiente"){
			Application.LoadLevel("Level_16_2");
		} else {
			life--;
			hit.Play ();
			if(life == 2 && !(Application.loadedLevelName.Equals("Level_16"))){
				life1.SetActive(false);
			}
			else if(life == 1 && !(Application.loadedLevelName.Equals("Level_16"))){
					life2.SetActive (false);
				}
			else if(life == 0 && !(Application.loadedLevelName.Equals("Level_16"))){
						life3.SetActive(false);
						movPlayer = false;
						gameOver.SetActive(true);
					}
			if(Application.loadedLevelName.Equals("Level_13")){
				this.gameObject.transform.position = new Vector2(-23.76f,12.59f);
				movAfterDeath = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				mov = new Vector2 (0, 0);
				movAfterDeath = false;
			}else if(Application.loadedLevelName.Equals("Level_6")){
				this.gameObject.transform.position = new Vector2(-2.36f,0);
				movAfterDeath = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				mov = new Vector2 (0, 0);
				movAfterDeath = false;
			}else if(Application.loadedLevelName.Equals("Level_8")){
				this.gameObject.transform.position = new Vector2(0,	3.31f);
				movAfterDeath = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				mov = new Vector2 (0, 0);
				movAfterDeath = false;
			} else if(Application.loadedLevelName.Equals("Level_9")){
				this.gameObject.transform.position = new Vector2(-23.43f,12.44f);
				movAfterDeath = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				mov = new Vector2 (0, 0);
				movAfterDeath = false;
			} else if(Application.loadedLevelName.Equals("Level_15")){
				this.gameObject.transform.position = new Vector2(-22.879f,11.62f);
				movAfterDeath = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				mov = new Vector2 (0, 0);
				movAfterDeath = false;
			}else{
				this.gameObject.transform.position = new Vector2(0,0);
				movAfterDeath = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = false;
				yield return new WaitForSeconds(0.2f);
				GetComponent<Renderer>().enabled = true;
				mov = new Vector2 (0, 0);
				movAfterDeath = false;
			}
		}
	}


	void Start(){
		iniSoundMain = true;
		life = 3;
		puntos = 0;
		espera = true;
		movPlayer = true;
		movAfterDeath = false;
		Quit.continuar = true;
		Time.timeScale=1;
		if (SystemInfo.systemMemorySize < 1024)
						vel = 12;
				else
						vel = 6;
		//loadingScene

		ChangeScene.level1Load = false;
		ChangeScene.level2Load = false;
		ChangeScene.level3Load = false;
		ChangeScene.level4Load = false;
		ChangeScene.level5Load = false;
		ChangeScene.level6Load = false;
		ChangeScene.level7Load = false;
		ChangeScene.level8Load = false;
	}

	void Update () {
		StartCoroutine (tiempoDeEspera ());
	}

	void movimiento(){
		if (Input.GetKey (KeyCode.RightArrow)) {
						mov = new Vector2 (vel * Time.deltaTime, 0);
						auxMov = mov;
				} else if (Input.GetKey (KeyCode.LeftArrow)) {
						mov = new Vector2 ((-1) * vel * Time.deltaTime, 0);
						auxMov = mov;
				} else if (Input.GetKey (KeyCode.UpArrow)) {
						mov = new Vector2 (0, vel * Time.deltaTime);
						auxMov = mov;
				} else if (Input.GetKey (KeyCode.DownArrow)) {
						mov = new Vector2 (0, (-1) * vel * Time.deltaTime);
						auxMov = mov;
				}


		// SWIPE
		if (!(Time.timeScale == 0)) {
				foreach (Touch touch in Input.touches) {
		
						if (touch.phase == TouchPhase.Began) {
								fp = touch.position;
								lp = touch.position;
						}
						if (touch.phase == TouchPhase.Moved) {
								lp = touch.position;
						}
						if (touch.phase == TouchPhase.Ended) {
								//First check if it's actually a drag
								if (Mathf.Abs (lp.x - fp.x) > dragDistance || Mathf.Abs (lp.y - fp.y) > dragDistance) {   //It's a drag
										//Now check what direction the drag was
										//First check which axis
										if (Mathf.Abs (lp.x - fp.x) > Mathf.Abs (lp.y - fp.y)) {   //If the horizontal movement is greater than the vertical movement...
												if (lp.x > fp.x) {  //If the movement was to the right   //Right move
													mov = new Vector2 (vel * Time.fixedDeltaTime , 0);
														auxMov = mov;
									
												} else {   //Left move
													mov = new Vector2 ((-1) * vel * Time.fixedDeltaTime , 0);
														auxMov = mov;
									
												}
										} else {   //the vertical movement is greater than the horizontal movement
												if (lp.y > fp.y) {  //If the movement was up   //Up move
													mov = new Vector2 (0, vel * Time.fixedDeltaTime );
														auxMov = mov;
							
												} else {   //Down move
													mov = new Vector2 (0, (-1) * vel * Time.fixedDeltaTime );
														auxMov = mov;

												}
										}
								} else {   //It's a tap
										//TAP CODE HERE
								}

						}
				}
		}


		if (movAfterDeath == true) {
				mov = new Vector2 (0.0f, 0.0f);	
		} else {
				if(!(Time.timeScale == 0))
					transform.Translate (mov);
		}

	}

	IEnumerator tiempoDeEspera(){
		if (espera && (RestartButton.noTimewait == false)) {
				mov = new Vector2 (0.0f, 0.0f);
				yield return new WaitForSeconds (2.5f);
				espera = false;
		} else if(movPlayer == true){
			if (Input.GetKey (KeyCode.Escape)) {
				quit.SetActive(true);
	        	Time.timeScale=0;
			}else if(RestartButton.reinicio == true){
				mov = new Vector2 (0.0f, 0.0f);
				yield return new WaitForSeconds (0.3F);
				RestartButton.reinicio = false;
	        }else{
				movimiento();
			}
		}
	}

	public void desbloquearNivel (){
		if(Application.loadedLevelName.Equals("Level_1"))
			level1Complete = true;
		else if(Application.loadedLevelName.Equals("Level_2"))
			level2Complete = true;
		else if(Application.loadedLevelName.Equals("Level_3"))
			level3Complete = true;
		else if(Application.loadedLevelName.Equals("Level_4"))
			level4Complete = true;
		else if(Application.loadedLevelName.Equals("Level_5"))
			level5Complete = true;
		else if(Application.loadedLevelName.Equals("Level_6"))
			level6Complete = true;
		else if(Application.loadedLevelName.Equals("Level_7"))
			level7Complete = true;
		else if(Application.loadedLevelName.Equals("Level_8"))
			level8Complete = true;
		else if(Application.loadedLevelName.Equals("Level_9"))
			level9Complete = true;
		else if(Application.loadedLevelName.Equals("Level_10"))
			level10Complete = true;
		else if(Application.loadedLevelName.Equals("Level_11"))
			level11Complete = true;
		else if(Application.loadedLevelName.Equals("Level_12"))
			level12Complete = true;
		else if(Application.loadedLevelName.Equals("Level_13"))
			level13Complete = true;
		else if(Application.loadedLevelName.Equals("Level_14"))
			level14Complete = true;
		else if(Application.loadedLevelName.Equals("Level_15"))
			level15Complete = true;
		Data.Save();
	}

}


