﻿using UnityEngine;
using System.Collections;

public class MovLvl12 : MonoBehaviour {

	bool m1,m2;
	public int speed = 20;
	public float pos1=21.89f;
	public float pos2=-21.89f;

	void Start () 
	{
		m1 = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!(RestartButton.parado))
		mov ();
	}

	void mov()
	{
		if (m1) 
		{
			this.gameObject.transform.Translate(Vector3.right*Time.deltaTime*speed, Space.World);
				if (this.gameObject.transform.position.x >= pos1) 
				{
					m1 = false;
					m2 = true;
				}
		}

		if(m2)
		{
			this.gameObject.transform.Translate(Vector3.left*Time.deltaTime*speed, Space.World);
				if (this.gameObject.transform.position.x<=pos2) 
				{
					m1 = true;
					m2 = false;
				} 
		}
	}

}
