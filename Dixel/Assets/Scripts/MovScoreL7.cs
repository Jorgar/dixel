﻿using UnityEngine;
using System.Collections;

public class MovScoreL7 : MonoBehaviour {

	GameObject score;
	Vector3 a;
	public int speed = 4;
	public float pos1,pos2,pos3,pos4;
	bool m1,m2,m3,m4;


	void Start () {
		m1 = true;
		m2 = false;
		m3 = false;
		m4 = false;
	}



	void Update(){
		movScore ();
	}

	void movScore(){
				if (m1) {
				this.gameObject.transform.Translate(Vector3.down*Time.deltaTime*speed, Space.World);
					if (this.gameObject.transform.position.y <= pos1) {
									m1 = false;
									m2 = true;
							} 
				}
				if(m2){
						this.gameObject.transform.Translate(Vector3.left*Time.deltaTime*speed, Space.World);
						if (this.gameObject.transform.position.x<pos2) {
								m2 = false;
								m3 = true;
							} 
				}
				if(m3){
					this.gameObject.transform.Translate(Vector3.up*Time.deltaTime*speed, Space.World);
					if (this.gameObject.transform.position.y> pos3) {
						m3 = false;
						m4 = true;
					} 
				}
				if(m4){
					this.gameObject.transform.Translate(Vector3.right*Time.deltaTime*speed, Space.World);
					if (this.gameObject.transform.position.x>pos4) {
						m4 = false;
						m1 = true;
					} 
				}
	}
}
