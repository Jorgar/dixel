﻿using UnityEngine;
using System.Collections;

public class MuteAll : MonoBehaviour {

	public static bool isMute;
	public tk2dUIToggleButton button;


	void Start(){
		Data.Load ();
		if (isMute == false)
						button.IsOn = true;
				else 
						button.IsOn = false;
	}

	public void mute(){
				if (isMute == false) {
						isMute = true;
						AudioListener.volume = 0.0f;
						Data.Save();
				} else {
						isMute = false;
						AudioListener.volume = 1.0f;
						Data.Save();
				}
		}
}
