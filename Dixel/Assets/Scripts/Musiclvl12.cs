﻿using UnityEngine;
using System.Collections;

public class Musiclvl12 : MonoBehaviour {


	// Use this for initialization
	void Awake () {
        if (RestartButton.siMusicalvl12)
        {
            DontDestroyOnLoad(this);
        }
        else if (!(Application.loadedLevelName.Equals("Level_7")))
        {
            Destroy(this.gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	}
}
