﻿using UnityEngine;
using System.Collections;

public class movDerecha : MonoBehaviour {

	bool m1,m2;
	bool interr;
	public float pos1,pos2;
	public float velocidad;
	public AudioSource cancion;

	void Start () {
		m1 = true; 
		interr = true;
	}
	
	// Update is called once per frame
	void Update () {
				if (m1) {
						this.gameObject.transform.Translate (Vector3.right * Time.deltaTime * velocidad, Space.World);
						if (this.gameObject.transform.position.x >= pos1) {
								m1 = false;
								m2 = true;
						}
				}
		
				if (m2) {
						this.gameObject.transform.Translate (Vector3.left * Time.deltaTime * velocidad, Space.World);
						if (this.gameObject.transform.position.x <= pos2) {
								m1 = true;
								m2 = false;
						} 
				}
		}
}
