﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {


    public AudioSource uno, dos, tres, cuatro, cinco, enter;
    AudioSource[] tecla;
    [Multiline(6)]
    public string frase, elReturn;
    public TextMesh mensaje1, mensaje2;
    public float segundos;
    public GameObject par, GameReturn, m2;
    public float separacion;
    public GameObject informacion;

    // Use this for initialization
    void Start()
    {
        tecla = new AudioSource[5];
        tecla[0] = uno;
        tecla[1] = dos;
        tecla[2] = tres;
        tecla[3] = cuatro;
        tecla[4] = cinco;
        StartCoroutine(escritura());

    }

    // Update is called once per frame
    void Update()
    {
    }

    IEnumerator escritura()
    {
        for (int i = 0; i < frase.Length; i++)
        {
            yield return new WaitForSeconds(segundos);
            mensaje1.text += frase[i];
            par.transform.position = new Vector2(par.transform.position.x + separacion, par.transform.position.y);
            //Debug.Log("tecleo");
            tecla[(int)Random.Range(0, 4)].Play();
        }

        enter.Play();
        yield return new WaitForSeconds(0.2f);
        informacion.SetActive(true);

        par.transform.position = new Vector2(-409.9f, -170);

        yield return new WaitForSeconds(0.25f);
        for (int i = 0; i < elReturn.Length; i++)
        {
            yield return new WaitForSeconds(segundos);
            mensaje2.text += elReturn[i];
            par.transform.position = new Vector2(par.transform.position.x + separacion, par.transform.position.y);
            //Debug.Log("tecleo");
            tecla[(int)Random.Range(0, 4)].Play();
        }

        GameReturn.SetActive(true);
        m2.SetActive(false);

    }
}