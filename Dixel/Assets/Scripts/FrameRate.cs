using UnityEngine;
using System.Collections;

public class FrameRate : MonoBehaviour {

	// Use this for initialization
	void Awake () {
		if(SystemInfo.systemMemorySize<1024){
			    Application.targetFrameRate = 30;

		}else {
			   Application.targetFrameRate = 60;
		}		
	}
}
