﻿using UnityEngine;
using System.Collections;

public class transInicio : MonoBehaviour {

	public GameObject fondo;
	public GameObject intAnim;
	tk2dSpriteAnimator anim;
	bool playing = false;

	// Use this for initialization
	void Start () {
		anim = intAnim.GetComponent<tk2dSpriteAnimator> ();
		Invoke ("destroyBackground", 1.33f);
		Invoke ("animacion", 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		if (playing)
				if (!(anim.IsPlaying("animOn")))
					DestroyObject (intAnim);
	}

	void destroyBackground()
	{
		DestroyObject (fondo);
	}

	void animacion()
	{
		intAnim.SetActive (true);
		playing = true;
		anim.Play ();
	}

}
