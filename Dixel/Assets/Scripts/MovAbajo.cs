﻿using UnityEngine;
using System.Collections;

public class MovAbajo : MonoBehaviour {

	bool m1,m2;
	public float pos1,pos2;
	public int velocidad;
	
	
	void Start () {
		m1 = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (m1) 
		{
			this.gameObject.transform.Translate(Vector3.down*Time.deltaTime*velocidad, Space.World);
			if (this.gameObject.transform.position.y <=pos1) 
			{
				m1 = false;
				m2 = true;
			}
		}
		
		if(m2)
		{
			this.gameObject.transform.Translate(Vector3.up*Time.deltaTime*velocidad, Space.World);
			if (this.gameObject.transform.position.y>= pos2) 
			{
				m1 = true;
				m2 = false;
			} 
		}
	}
}
