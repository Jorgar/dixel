﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Lvl16 : MonoBehaviour {

    public tk2dSpriteAnimator anim;
    public AudioSource son;
    public GameObject spr,uno,dos,tres,cuatro;
    public float segundos1,segundos2;


    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        StartCoroutine(animacion());
	}
	
	// Update is called once per frame
	void Update () {
	    
	}



    IEnumerator animacion()
    {
        yield return new WaitForSeconds(1f);
        tres.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        tres.SetActive(false);
        son.Play();
        cuatro.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        son.Stop();
        cuatro.SetActive(false);
        yield return new WaitForSeconds(1f);
        uno.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        uno.SetActive(false);
        son.Play();
        dos.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        son.Stop();
        dos.SetActive(false);
        yield return new WaitForSeconds(segundos1);
        spr.SetActive(true);
        anim.Play();
        son.loop = true;
        son.Play();
        yield return new WaitForSeconds(segundos2);
        SceneManager.LoadScene("Level_16_4");
    }
}
