﻿using UnityEngine;
using System.Collections;

public class MusicaLvl16Nodes : MonoBehaviour {

	public AudioSource mus;
	bool empezar; 

	void Start () {
		if (Application.loadedLevelName.Equals ("Level_16")) 
				DontDestroyOnLoad (this.gameObject);
		empezar = true;

	}

	void Update(){
		if (!(Application.loadedLevelName.Equals ("Level_16_2") || Application.loadedLevelName.Equals ("Level_16")))
						Destroy (this.gameObject);
		if (empezar) {
						if (Application.loadedLevelName.Equals ("Level_16_2")) {
								StartCoroutine (musica ());
						}
				}
	}
	

	IEnumerator musica(){
		mus.volume = 0.8f;
		yield return new WaitForSeconds(0.25f);
		mus.volume = 0.6f;
		yield return new WaitForSeconds(0.25f);
		mus.volume = 0.4f;
		yield return new WaitForSeconds(0.25f);
		mus.volume = 0.2f;
		yield return new WaitForSeconds(1f);
		mus.volume = 0.0f;
		empezar = false;
	}
}
