﻿using UnityEngine;
using System.Collections;

public class ReturnToL1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape))
			if(Application.loadedLevelName.Equals("03_Levels02"))
				Application.LoadLevel ("03_Levels01");
			else if(Application.loadedLevelName.Equals("03_Levels03"))
				Application.LoadLevel ("03_Levels02");
			else if(Application.loadedLevelName.Equals("03_Levels04"))
				Application.LoadLevel ("03_Levels03");
	}
}
