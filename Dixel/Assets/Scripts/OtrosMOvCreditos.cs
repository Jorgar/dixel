﻿using UnityEngine;
using System.Collections;

public class OtrosMOvCreditos : MonoBehaviour {

	public float esperarAntes;
	public float esperarDespues;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (DerechaOtrosCreditos.empezaCreditos) {
			StartCoroutine(esperar ());
		}
	}
	
	IEnumerator esperar (){
		yield return new WaitForSeconds (esperarAntes);
		if(this.gameObject.transform.localPosition.x >= -5.6)
			this.gameObject.transform.Translate(new Vector2((-1)*DerechaOtrosCreditos.velocidad*Time.deltaTime*5, 0));
		yield return new WaitForSeconds (esperarDespues);
		if(this.gameObject.transform.localPosition.x >=(-60))
			this.gameObject.transform.Translate(new Vector2((-1)*DerechaOtrosCreditos.velocidad*Time.deltaTime*5, 0));
		DerechaCreditosFinal.empezaCreditos = false;
	}
}
