﻿using UnityEngine;
using System.Collections;

public class TeclearLoad1 : MonoBehaviour {

    public AudioSource uno, dos, tres, cuatro, cinco, enter;
    AudioSource[] tecla = new AudioSource[5];
    [Multiline(6)]
    public string frase, elReturn,conitnuar;
    string cuentaLoad;
    public TextMesh mensaje1, mensaje2,loading,mensaje3;
    public float segundos;
    public GameObject par, m2;
    public float separacion;
    public GameObject informacion,barra,barra2;
    bool tap;

    // Use this for initialization
    void Start()
    {
        tap = false;
        tecla[0] = uno;
        tecla[1] = dos;
        tecla[2] = tres;
        tecla[3] = cuatro;
        tecla[4] = cinco;
        StartCoroutine(escritura());

    }

    // Update is called once per frame
    void Update()
    {
        if (tap) { 
            if (Input.GetMouseButtonDown(0))
            {
                Application.LoadLevel("Level_1");
            }
        }
    }

    IEnumerator escritura()
    {
        for (int i = 0; i < frase.Length; i++)
        {
            yield return new WaitForSeconds(segundos);
            mensaje1.text += frase[i];
            par.transform.position = new Vector2(par.transform.position.x + separacion, par.transform.position.y);
            //Debug.Log("tecleo");
            tecla[(int)Random.Range(0, 4)].Play();
        }

        enter.Play();
        yield return new WaitForSeconds(0.2f);
        informacion.SetActive(true);

        par.transform.position = new Vector2(-422, -103);

        yield return new WaitForSeconds(0.25f);
        for (int i = 0; i < elReturn.Length; i++)
        {
            yield return new WaitForSeconds(segundos);
            mensaje2.text += elReturn[i];
            par.transform.position = new Vector2(par.transform.position.x + separacion, par.transform.position.y);
            //Debug.Log("tecleo");
            tecla[(int)Random.Range(0, 4)].Play();
        }

        enter.Play();
        barra.SetActive(true);
        par.transform.position = new Vector2(-355, -160);

        for(int i=1; i <= 100; i++)
        {
            cuentaLoad = i + "%";
            loading.text = cuentaLoad;
            yield return new WaitForSeconds(0.025f);
        }

        barra2.SetActive(true);
        par.transform.position = new Vector2(-422, -220);
        yield return new WaitForSeconds(0.25f);

        for (int i = 0; i < conitnuar.Length; i++)
        {
            yield return new WaitForSeconds(segundos);
            mensaje3.text += conitnuar[i];
            par.transform.position = new Vector2(par.transform.position.x + separacion, par.transform.position.y);
            //Debug.Log("tecleo");
            tecla[(int)Random.Range(0, 4)].Play();
        }

        tap = true;
        
 

    }
}
