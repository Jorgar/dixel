﻿using UnityEngine;
using System.Collections;

public class Desbloqueo : MonoBehaviour {

	public  GameObject level1, level1Complete;
	public  GameObject level2Locked, level2, level2Complete;
	public  GameObject level3Locked, level3, level3Complete;
	public  GameObject level4Locked, level4, level4Complete;

	void Start () {
		if (Controlador.level1Complete == true) {
						level1.SetActive (false);
						level2Locked.SetActive(false);
				}
		if (Controlador.level2Complete == true) {
			level2.SetActive (false);
			level3Locked.SetActive(false);
			level3.SetActive(true);	
		}

		if (Controlador.level3Complete == true) {
			level3.SetActive (false);
			level3Complete.SetActive (true);
			level4Locked.SetActive(false);
			level4.SetActive(true);	
		}
		if (Controlador.level4Complete == true) {
			level4.SetActive(false);
			level4Complete.SetActive(true);
		}

	}
}
