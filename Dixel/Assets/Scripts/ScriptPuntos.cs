﻿using UnityEngine;
using System.Collections;

public class ScriptPuntos : MonoBehaviour {

	public int velocidadGiro;

	void Update () {
		transform.Rotate(Vector3.forward * Time.deltaTime * velocidadGiro);
	}
}
