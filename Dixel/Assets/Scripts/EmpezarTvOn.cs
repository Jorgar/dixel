﻿using UnityEngine;
using System.Collections;

public class EmpezarTvOn : MonoBehaviour {

    public float segundos;
    
    // Update is called once per frame
    void Update () {
        //StartCoroutine(startTvOn());
     
	}

    void Start()
    {
        Invoke("empezar", segundos);
    }



    void empezar()
    {
        this.gameObject.GetComponent<AudioSource>().Play();
    }

    IEnumerator startTvOn()
    {
        yield return new WaitForSeconds(segundos);
        this.gameObject.GetComponent<AudioSource>().Play();
    }
}
