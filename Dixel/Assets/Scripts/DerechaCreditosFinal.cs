﻿using UnityEngine;
using System.Collections;

public class DerechaCreditosFinal : MonoBehaviour {
	bool interr;
	public static bool empezaCreditos;
	public static float velocidad;
	public AudioSource cancion;
	
	void Start () {
		interr = true;
		empezaCreditos = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (!(this.gameObject.transform.position.x >= 0)) {
				if (SystemInfo.systemMemorySize < 1024){
						velocidad = 12;
						this.gameObject.transform.Translate(Vector3.right*Time.fixedDeltaTime*velocidad, Space.World);
				}else{
					velocidad = 6;
					this.gameObject.transform.Translate(Vector3.right*Time.fixedDeltaTime*velocidad, Space.World);
				}
		}

		if (Application.loadedLevelName.Equals ("Level_16_2") && interr)
			if (this.gameObject.transform.position.x >= 0) {		
			StartCoroutine(cancionM());
			interr = false;
		}
		
	}
	
	IEnumerator cancionM(){
		yield return new WaitForSeconds (1.5f);
		cancion.Play ();
		empezaCreditos = true;
	}

}
