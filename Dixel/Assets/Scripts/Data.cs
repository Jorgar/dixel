﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Data : MonoBehaviour {

	public static void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

		PlayerData data = new PlayerData ();
		data.level1Complete = Controlador.level1Complete;
		data.level2Complete = Controlador.level2Complete;
		data.level3Complete = Controlador.level3Complete;
		data.level4Complete = Controlador.level4Complete;
		data.level5Complete = Controlador.level5Complete;
		data.level6Complete = Controlador.level6Complete;
		data.level7Complete = Controlador.level7Complete;
		data.level8Complete = Controlador.level8Complete;
		data.level9Complete = Controlador.level9Complete;
		data.level10Complete = Controlador.level10Complete;
		data.level11Complete = Controlador.level11Complete;
		data.level12Complete = Controlador.level12Complete;
		data.level13Complete = Controlador.level13Complete;
		data.level14Complete = Controlador.level14Complete;
		data.level15Complete = Controlador.level15Complete;
		data.firstLoad1 = ChangeScene.level1FirstLoad;
		data.isMute = MuteAll.isMute;

		bf.Serialize (file, data);
		file.Close ();
	}

	public static void Load(){
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close ();

			Controlador.level1Complete = data.level1Complete;
			Controlador.level2Complete = data.level2Complete;
			Controlador.level3Complete = data.level3Complete;
			Controlador.level4Complete = data.level4Complete;
			Controlador.level5Complete = data.level5Complete;
			Controlador.level6Complete = data.level6Complete;
			Controlador.level7Complete = data.level7Complete;
			Controlador.level8Complete = data.level8Complete;
			Controlador.level9Complete = data.level9Complete;
			Controlador.level10Complete= data.level10Complete ;
			Controlador.level11Complete = data.level11Complete;
			Controlador.level12Complete = data.level12Complete;
			Controlador.level13Complete = data.level13Complete;
			Controlador.level14Complete = data.level14Complete;
			Controlador.level15Complete = data.level15Complete;
			ChangeScene.level1FirstLoad = data.firstLoad1;
			MuteAll.isMute = data.isMute;
		}
	}
}

[Serializable]
class PlayerData{

	public bool level1Complete,level2Complete,level3Complete,level4Complete
		,level5Complete,level6Complete,level7Complete,level8Complete
		,level9Complete,level10Complete,level11Complete,level12Complete
		,level13Complete,level14Complete,level15Complete;

	public bool firstLoad1; 
	public bool isMute; 

}
