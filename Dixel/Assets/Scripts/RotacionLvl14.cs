﻿using UnityEngine;
using System.Collections;

public class RotacionLvl14 : MonoBehaviour {

	//public GameObject uno, dos, tres, cuatro;

	void Start () {
		StartCoroutine (rot ());
	}


	void rotacion(){
		Invoke ("primero", 1);
		Invoke ("segundo", 2);
		Invoke ("tercero", 3);
		Invoke ("cuarto", 4);
	}

	void primero(){
		//uno.SetActive (false);
		//dos.SetActive (true);
		this.transform.Rotate (0, 0, 90, Space.World);
	}

	void segundo(){
		//dos.SetActive (false);
		//tres.SetActive(true);
		this.transform.Rotate (0, 0, 180, Space.World);
	}

	void tercero(){
		//tres.SetActive (false);
		//cuatro.SetActive (true);
		this.transform.eulerAngles = new Vector3(0,0,270);
	}

	void cuarto(){
		//cuatro.SetActive (false);
		//uno.SetActive (true);
		this.transform.eulerAngles = new Vector3(0,0,0);
	}

	IEnumerator rot(){
		yield return new WaitForSeconds(1);
		this.transform.eulerAngles = new Vector3(0,0,90);
		yield return new WaitForSeconds(1);
		this.transform.eulerAngles = new Vector3(0,0,180);
		yield return new WaitForSeconds(1);
		this.transform.eulerAngles = new Vector3(0,0,270);
		yield return new WaitForSeconds(1);
		this.transform.eulerAngles = new Vector3(0,0,0);
		StartCoroutine (rot ());
	}
}






