﻿using UnityEngine;
using System.Collections;

public class MenuMusic : MonoBehaviour {

	public static bool iniSoundMain = false;
	static bool AudioBegin = false; 
	static bool empezar = false;
	void Awake()
	{
				if (!(Application.loadedLevel == 1)) {
						if (!AudioBegin) {
								empezar = true;
								//audio.Play ();
								DontDestroyOnLoad (gameObject);
								AudioBegin = true;
						}
								
				} else {
							if (!AudioBegin && Controlador.iniSoundMain == true) {
									GetComponent<AudioSource>().Play ();
									DontDestroyOnLoad (gameObject);
									AudioBegin = true;
									
							} 
				}
	}
		


	void Start(){
		if (empezar) {
						empezar = false;
						StartCoroutine (esperar ());
		} 
		Controlador.iniSoundMain = false;	
	}

	void Update () {
		if(Application.loadedLevelName == "Level_1" || Application.loadedLevelName == "Level_2" || Application.loadedLevelName == "Level_3"
		   || Application.loadedLevelName == "Level_4" || Application.loadedLevelName == "Level_5" || Application.loadedLevelName == "Level_6" 
		   || Application.loadedLevelName == "Level_7" || Application.loadedLevelName == "Level_8" || Application.loadedLevelName == "Level_9" 
		   || Application.loadedLevelName == "Level_10"|| Application.loadedLevelName == "Level_11" || Application.loadedLevelName == "Level_12"
		   || Application.loadedLevelName == "Level_13"|| Application.loadedLevelName == "Level_14"|| Application.loadedLevelName == "Level_15"
		   || Application.loadedLevelName == "Level_16")
		{
			GetComponent<AudioSource>().Stop();
			AudioBegin = false;
			Destroy (this.gameObject);
		}
	}

	IEnumerator esperar(){
				yield return new WaitForSeconds (1.5f);
				GetComponent<AudioSource>().Play ();
	}
}
