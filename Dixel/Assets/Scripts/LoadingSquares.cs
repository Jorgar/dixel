﻿using UnityEngine;
using System.Collections;

public class LoadingSquares : MonoBehaviour {

	public GameObject square1, square2, square3;
	int contador = 0;
	int contador2 = 0;


	void Start () {
		Time.timeScale = 1;
		square1.SetActive (false);
		square2.SetActive (false);
		square3.SetActive (false);
		StartCoroutine(tiempoSquare());

	}
	
	// Update is called once per frame
	void Update () {

	}

	public IEnumerator tiempoSquare(){
			while (contador==0) {
				square1.SetActive (true);
				yield return new WaitForSeconds (0.3f);
				square2.SetActive (true);
				yield return new WaitForSeconds (0.3f);
				contador2++;
				if (contador2 == 2) {
					if (ChangeScene.level1Load)
					{	
						ChangeScene.level1Load=false;
						Application.LoadLevel ("Level_1");
					}else if (ChangeScene.level2Load)
					{
						ChangeScene.level2Load=false;
						Application.LoadLevel ("Level_2");
					}
					else if (ChangeScene.level3Load)
					{
						ChangeScene.level3Load=false;
						Application.LoadLevel ("Level_3");
					}
					else if (ChangeScene.level4Load)
					{
						ChangeScene.level4Load=false;
						Application.LoadLevel ("Level_4");
					}
					else if (ChangeScene.level5Load)
					{
						ChangeScene.level5Load=false;
						Application.LoadLevel ("Level_5");
					}
					else if (ChangeScene.level6Load)
					{
						ChangeScene.level6Load=false;
						Application.LoadLevel ("Level_6");
					}
					else if (ChangeScene.level7Load)
					{
						ChangeScene.level7Load=false;
						Application.LoadLevel ("Level_7");
					}
					else if (ChangeScene.level8Load)
					{
						ChangeScene.level8Load=false;
						Application.LoadLevel ("Level_8");
					}
					else if (ChangeScene.level9Load)
					{
						ChangeScene.level9Load=false;
						Application.LoadLevel ("Level_9");
					}
					else if (ChangeScene.level10Load)
					{
						ChangeScene.level10Load=false;
						Application.LoadLevel ("Level_10");
					}
					else if (ChangeScene.level11Load)
					{
						ChangeScene.level11Load=false;
						Application.LoadLevel ("Level_11");
					}
					else if (ChangeScene.level12Load)
					{
						ChangeScene.level12Load=false;
						Application.LoadLevel ("Level_12");
					}
					else if (ChangeScene.level13Load)
					{
						ChangeScene.level13Load=false;
						Application.LoadLevel ("Level_13");
					}
					else if (ChangeScene.level14Load)
					{
						ChangeScene.level14Load=false;
						Application.LoadLevel ("Level_14");
					}
					else if (ChangeScene.level15Load)
					{
						ChangeScene.level15Load=false;
						Application.LoadLevel ("Level_15");
					}
					else if (ChangeScene.level16Load)
					{
						ChangeScene.level16Load=false;
						Application.LoadLevel ("Level_16");
					}


				}
				square3.SetActive (true);
				yield return new WaitForSeconds (0.3f);
				square1.SetActive (false);
				square2.SetActive (false);
				square3.SetActive (false);
				yield return new WaitForSeconds (0.3f);

		}
				
	}
}
