﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RestartButton : MonoBehaviour
{

    public GameObject life1;
    public GameObject life2;
    public GameObject life3;
    public GameObject jugador;
    public GameObject gameOver;
    public GameObject[] objetosPuntos;
    GameObject auxPoint;
    public GameObject punto1, punto2, punto3, punto4, punto5,
                punto6, punto7, punto8, punto9, punto10;
    public static bool reinicio = false;
    public static bool solBug12 = false;
    public static bool parado = false;
    public GameObject bloque1, bloque2, bloque3, bloque4, bloque5, bloque6;
    public static bool siMusicalvl12;
    public static bool noCabecera;
    public static bool noTimewait = false;


    public void buttonRestart()
    {
        life1.SetActive(true);
        life2.SetActive(true);
        life3.SetActive(true);
        Controlador.life = 3;
        Controlador.puntos = 0;
        reinicio = true;
        if (Application.loadedLevelName.Equals("Level_13"))
        {
            jugador.gameObject.transform.position = new Vector2(-23.76f, 12.59f);
        }
        else if (Application.loadedLevelName.Equals("Level_6"))
        {
            jugador.gameObject.transform.position = new Vector2(-2.34f, 0);
        }
        else if (Application.loadedLevelName.Equals("Level_8"))
        {
            jugador.gameObject.transform.position = new Vector2(0, 3.31f);
        }
        else if (Application.loadedLevelName.Equals("Level_9"))
        {
            jugador.gameObject.transform.position = new Vector2(-23.43f, 12.44f);
        }
        else if (Application.loadedLevelName.Equals("Level_15"))
        {
            jugador.gameObject.transform.position = new Vector2(-22.879f, 11.62f);
        }
        else {
            jugador.gameObject.transform.position = new Vector2(0, 0);
        }

            punto1.SetActive(true);
            punto2.SetActive(true);
            punto3.SetActive(true);
            punto4.SetActive(true);
            punto5.SetActive(true);
            punto6.SetActive(true);
            punto7.SetActive(true);
            punto8.SetActive(true);
            punto9.SetActive(true);
            punto10.SetActive(true);

            Controlador.movPlayer = true;
            gameOver.SetActive(false);
        
    }
}
