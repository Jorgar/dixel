﻿using UnityEngine;
using System.Collections;

public class MovCreditsOpt : MonoBehaviour {

	public float esperarAntes;
	public float esperarDespues;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (DerechaCreditosFinal.empezaCreditos) {
			StartCoroutine(esperar ());
		}
			
	}
	
	IEnumerator esperar (){
		yield return new WaitForSeconds (esperarAntes);
		if(this.gameObject.transform.position.x >=0)
			this.gameObject.transform.Translate(new Vector2((-1)*DerechaCreditosFinal.velocidad*Time.deltaTime*5, 0));
		yield return new WaitForSeconds (esperarDespues);
		if(this.gameObject.transform.position.x >=(-800))
			this.gameObject.transform.Translate(new Vector2((-1)*DerechaCreditosFinal.velocidad*Time.deltaTime*5, 0));
		DerechaCreditosFinal.empezaCreditos = false;
	}
}
