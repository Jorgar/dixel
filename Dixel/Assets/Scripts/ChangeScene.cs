﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

	public static bool level1Load = false;
	public static bool level2Load = false;
	public static bool level3Load = false;
	public static bool level4Load = false;
	public static bool level5Load = false;
	public static bool level6Load = false;
	public static bool level7Load = false;
	public static bool level8Load = false;
	public static bool level9Load = false;
	public static bool level10Load = false;
	public static bool level11Load = false;
	public static bool level12Load = false;
	public static bool level13Load = false;
	public static bool level14Load = false;
	public static bool level15Load = false;
	public static bool level16Load = false;
	public static bool level1FirstLoad = true;


	public void toMainMenu(){
        SceneManager.LoadScene("01_Main");
        StopAllCoroutines();
	}

	public void toOptions(){
        SceneManager.LoadScene("02_Options");
        StopAllCoroutines();
    }

	public void toLevels01(){
        SceneManager.LoadScene("03_Levels01");
        StopAllCoroutines();
    }

	public void toLevels02(){
        SceneManager.LoadScene("03_Levels02");
        StopAllCoroutines();
    }

	public void toLevels03(){
        SceneManager.LoadScene("03_Levels03");
        StopAllCoroutines();
    }

	public void toLevels04(){
        SceneManager.LoadScene("03_Levels04");
        StopAllCoroutines();
    }

	public void toLevel1(){
		level1Load = true;
		if (level1FirstLoad == true) {
              SceneManager.LoadScene("Loading02");
              StopAllCoroutines();
        } else {
             SceneManager.LoadScene("Loading02");
             StopAllCoroutines();
        }
	}

	public void toLevel2(){
        StopAllCoroutines();
        level2Load = true;
        SceneManager.LoadScene("Loading01");
    }

	public void toLevel3(){
		level3Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }
	public void toLevel4(){
		level4Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel5(){
		level5Load = true;
        SceneManager.LoadScene("Loading01");;
        StopAllCoroutines();
    }
	
	public void toLevel6(){
		level6Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }
	
	public void toLevel7(){
		level7Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel8(){
		level8Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel9(){
		level9Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel10(){
		level10Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel11(){
		level11Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel12(){
		level12Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel13(){
		level13Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel14(){
		level14Load = true;
        SceneManager.LoadScene("Loading01");
        StopAllCoroutines();
    }

	public void toLevel15(){
        StopAllCoroutines();
		level15Load = true;
        SceneManager.LoadScene("Loading01");
        
    }

	public void toLevel16(){
        StopAllCoroutines();
		level16Load = true;
        SceneManager.LoadScene("Loading01");
        
    }

	//Pruebas
	public void toPruebas(){
        SceneManager.LoadScene("_Pruebas");
	}

	public void toCredits(){
        StopAllCoroutines();
        SceneManager.LoadScene("Credits");
        
    }

}
