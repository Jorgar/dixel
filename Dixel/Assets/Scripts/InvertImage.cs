﻿using UnityEngine;
using System.Collections;

public class InvertImage : MonoBehaviour {

	private static bool invert = false;
	public tk2dUIToggleButton button;
	// Use this for initialization
	void Start () {
		if (invert == false)
			button.IsOn = true;
		else 
			button.IsOn = false;
	}
	
	// Update is called once per frame
	void invertImage () {
		if (invert == false) {
			invert = true;
			Debug.Log ("Blanco");
		} else {
			invert = false;
			Debug.Log ("Negro");
		}
	}
}
