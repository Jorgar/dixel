﻿using UnityEngine;
using System.Collections;

public class Destruccion : MonoBehaviour {
	 
	public float segundos;
	public static bool ya;
	// Use this for initialization
	void Start () {
		StartCoroutine (espera ());
	}
	
	// Update is called once per frame
	void Update () {

	}

	IEnumerator espera(){
		yield return new WaitForSeconds (segundos);
		this.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
		ya = true;

	}
}
