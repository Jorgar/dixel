﻿using UnityEngine;
using System.Collections;

public class Swipe : MonoBehaviour {

	public int vel;

	private Vector3 fp; //First finger position
	private Vector3 lp; //Last finger position
	public float dragDistance;  //Distance needed for a swipe to register

	void Update () {

		foreach (Touch touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Began)
			{
				fp = touch.position;
				lp = touch.position;
			}
			if (touch.phase == TouchPhase.Moved)
			{
				lp = touch.position;
			}
			if (touch.phase == TouchPhase.Ended)
			{
				//First check if it's actually a drag
				if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
				{   //It's a drag
					//Now check what direction the drag was
					//First check which axis
					if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
					{   //If the horizontal movement is greater than the vertical movement...
						if (lp.x>fp.x)  //If the movement was to the right
						{   //Right move
							Controlador.mov = new Vector2 (vel * Time.deltaTime, 0);
						}
						else
						{   //Left move
							Controlador.mov = new Vector2 ((-1) * vel * Time.deltaTime, 0);
						}
					}
					else
					{   //the vertical movement is greater than the horizontal movement
						if (lp.y>fp.y)  //If the movement was up
						{   //Up move
							Controlador.mov = new Vector2 (0, vel * Time.deltaTime);
						}
						else
						{   //Down move
							Controlador.mov = new Vector2 (0, (-1) * vel * Time.deltaTime);
						}
					}
				}
				else
				{   //It's a tap
					//TAP CODE HERE
				}
				
			}
		}

	}
}
