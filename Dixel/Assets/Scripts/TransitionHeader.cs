﻿using UnityEngine;
using System.Collections;

public class TransitionHeader : MonoBehaviour {

	public SpriteRenderer sprite;


	void Start () {
		StartCoroutine(Transicion());
	}
	

	void Update () {

	}

	IEnumerator Transicion(){
				if (RestartButton.noCabecera == false) {	
						//APARECE
						sprite.color = new Color (1f, 1f, 1f, 0.0f); 
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.1f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.2f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.3f); 
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.4f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.5f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.6f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.7f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.8f); 
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.9f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 1f);
						yield return new WaitForSeconds (1f);
						//DESAPARECE
						sprite.color = new Color (1f, 1f, 1f, 1f); 
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.9f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.8f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.7f); 
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.6f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.5f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.4f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.3f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.2f); 
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.1f);
						yield return new WaitForSeconds (0.05f);
						sprite.color = new Color (1f, 1f, 1f, 0.0f);
						Destroy (this.gameObject);
				} else {
						Destroy (this.gameObject);
				}
		}
}
