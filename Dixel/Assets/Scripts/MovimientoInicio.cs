﻿using UnityEngine;
using System.Collections;

public class MovimientoInicio : MonoBehaviour {

	Vector2 movStart;
	public int velocidad;
	Vector2 izquierda = new Vector2 (-1, 0);
	Vector2 derecha = new Vector2 (1, 0);
	Vector2 arriba = new Vector2 (0, 1);
	Vector2 abajo = new Vector2 (0, -1);
	public GameObject cuboInicio;
	bool tiempoDeEspera = true;
	bool empieza = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (tiempoDeEspera == true) {
						StartCoroutine ("tiempoEsperaInicio");
				} else {
						MovimientoStart ();
				}
		//StartCoroutine (tiempoEsperaInicio ());
	}

	void MovimientoStart(){
		if (empieza) {
			movStart = derecha * Time.deltaTime*velocidad;
			if(cuboInicio.transform.position.x>745)
				empieza = false;
		}
		if (cuboInicio.transform.position.x > 1000 && cuboInicio.transform.position.y < 40) {
						movStart = arriba * Time.deltaTime * velocidad;
		}else if(cuboInicio.transform.position.y > 600 && cuboInicio.transform.position.x > 500){
		         movStart = izquierda * Time.deltaTime * velocidad;
		} else if (cuboInicio.transform.position.x < 504 && cuboInicio.transform.position.y > 400) {
				movStart = abajo * Time.deltaTime * velocidad;
		}

		cuboInicio.transform.Translate (movStart);
	}

	IEnumerator tiempoEsperaInicio(){
			yield return new WaitForSeconds (50.0f);	
			tiempoDeEspera = false;
		}
}

