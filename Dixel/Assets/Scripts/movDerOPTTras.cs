﻿using UnityEngine;
using System.Collections;

public class movDerOPTTras : MonoBehaviour {

	public GameObject esto;
	// Use this for initialization
	void Start () {
        posiTest.yaEsta = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName.Equals("Credits")){
		if(posiTest.yaEsta){
			this.gameObject.transform.Translate(Vector3.right*Time.fixedDeltaTime*DerechaOtrosCreditos.velocidad, Space.World);
			if(this.gameObject.transform.position.x >=30){
				StartCoroutine(tituloA ());
				posiTest.yaEsta = false;
			}
		}
		}
	}

	IEnumerator tituloA(){
		esto.SetActive (true);
		yield return new WaitForSeconds(5);
		esto.SetActive (false);
		yield return new WaitForSeconds(1);
		Application.LoadLevel ("02_Options");
	}

}
