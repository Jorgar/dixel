﻿using UnityEngine;
using System.Collections;

public class ParpadeoConsola : MonoBehaviour {

    public GameObject par;
    Renderer rend; 
    public float segundosParpadeo;

	// Use this for initialization
	void Start () {
        StopAllCoroutines();
        rend = par.GetComponent<Renderer>();
        StartCoroutine(parpadeo());
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator parpadeo()
    {
        yield return new WaitForSeconds(segundosParpadeo);
        rend.enabled = false;
        yield return new WaitForSeconds(segundosParpadeo);
        rend.enabled = true;
        StartCoroutine(parpadeo());
    }
}
