﻿using UnityEngine;
using System.Collections;

public class AparecerLvl16 : MonoBehaviour {

	public AudioSource apparecer;

	// Use this for initialization
	void Start () {
		StartCoroutine (esperar ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator esperar (){
		yield return new WaitForSeconds (7.5f);
		this.gameObject.transform.position = new Vector3 (0, 0, 0);
		apparecer.Play ();
	}
}
