﻿using UnityEngine;
using System.Collections;

public class PlayFinalLevelRobot : MonoBehaviour {

	public GameObject glitchUno,glitchDos;
	public AudioSource desconexion; 
	public AudioSource glitch; 
	// Use this for initialization
	void Start () {
		glitchUno.SetActive (false);
		glitchDos.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Destruccion.ya) {
			desconexion.Play();
			Destruccion.ya = false;
			glitch.Play ();
			StartCoroutine(glitchMet ());
		}
	}

	IEnumerator glitchMet(){
		yield return new WaitForSeconds(Random.Range(2,5));
		glitchUno.SetActive (true);
		yield return new WaitForSeconds(0.10f);
		glitchDos.SetActive (true);
		yield return new WaitForSeconds(0.05f);
		glitchUno.SetActive (false);
		glitchDos.SetActive (false);
		StartCoroutine (glitchMet ());
	}
}
