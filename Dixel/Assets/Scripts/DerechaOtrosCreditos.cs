﻿using UnityEngine;
using System.Collections;

public class DerechaOtrosCreditos : MonoBehaviour {

		bool interr;
		public static bool empezaCreditos;
		public static float velocidad;
		public AudioSource cancion;
		public static Vector2 posMuro;
		
		void Start () {
			empezaCreditos = false;
			interr = true;
			empezaCreditos = false;
			posiTest.yaEsta = false;
		}
		
		// Update is called once per frame
		void Update () {
			
			if (!(this.gameObject.transform.position.x >= Mathf.Clamp(0,0,0))) {
				if (SystemInfo.systemMemorySize < 1024){
					velocidad = 12;
					this.gameObject.transform.Translate(Vector3.right*Time.fixedDeltaTime*velocidad, Space.World);
				}else{
					velocidad = 6;
					this.gameObject.transform.Translate(Vector3.right*Time.fixedDeltaTime*velocidad, Space.World);
				}
			}
			
			if ((Application.loadedLevelName.Equals ("Level_16_2") || Application.loadedLevelName.Equals ("Credits")) && interr)
			if (this.gameObject.transform.position.x >= Mathf.Clamp(0,0,0)) {	
				posMuro = new Vector2(this.gameObject.transform.position.x,this.gameObject.transform.position.y);
				StartCoroutine(cancionM());
				interr = false;
			}
			if (OtroScript162.bajarvolumen)
						StartCoroutine (volumen ());
			
		}
		
		IEnumerator cancionM(){
			yield return new WaitForSeconds (1.5f);
			if(Application.loadedLevelName.Equals("Level_16_2"))
				cancion.Play ();	
			empezaCreditos = true;
		}

		IEnumerator volumen(){
			cancion.volume = 0.8f;
			yield return new WaitForSeconds (0.5f);
			cancion.volume = 0.4f;
			yield return new WaitForSeconds (0.5f);
			cancion.volume = 0.0f;
			
		}
		
	}
