﻿using UnityEngine;
using System.Collections;

public class Leftmovlvl12 : MonoBehaviour {
	bool m1,m2;
	public int speed = 20;
	public float pos1=-21.89f;
	public float pos2=21.89f;
	bool stop = false;
	
	void Start () 
	{
		m1 = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!(RestartButton.parado))
		mov ();
	}
	
	void mov()
	{
		if (m1) 
		{
			this.gameObject.transform.Translate(Vector3.left*Time.deltaTime*speed, Space.World);
			if (this.gameObject.transform.position.x <= pos1) 
			{	
				stop = true;
				m1 = false;
				m2 = true;
				stop = false;
			}
		}
		
		if(m2)
		{
			this.gameObject.transform.Translate(Vector3.right*Time.deltaTime*speed, Space.World);
			if (this.gameObject.transform.position.x>=pos2) 
			{
				stop = true;
				m1 = true;
				m2 = false;
				stop = false;

			} 
		}
	}
	
}